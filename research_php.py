#!/usr/bin/env python3

import os
import re
import argparse
import logging
from pathlib import Path

# TODO: add flag for md vs opml vs txt
# TODO: add verbosity flag
logging.basicConfig(level=logging.WARNING)

COMMON_PHP_BUILT_INS = [
    'echo', 'print', 'var_dump', 'isset', 'empty', 'array', 'count', 'strlen', 'trim', 'explode', 'implode',
    'substr', 'str_replace', 'preg_replace', 'preg_match', 'strtolower', 'strtoupper', 'strval', 'intval',
    'floatval', 'round', 'floor', 'ceil', 'date', 'time', 'strtotime', 'microtime', 'sprintf', 'json_encode',
    'json_decode', 'serialize', 'unserialize', 'urlencode', 'urldecode', 'md5', 'sha1', 'password_hash',
    'password_verify', 'crypt', 'base64_encode', 'base64_decode', 'ord', 'chr', 'addslashes', 'stripslashes',
    'str_pad', 'str_repeat', 'str_split', 'strstr', 'stristr', 'strrchr', 'strpos', 'stripos', 'strrpos', 'strripos',
    'str_word_count', 'strcspn', 'strspn', 'strcoll', 'substr_replace', 'substr_count', 'strnatcmp', 'strnatcasecmp',
    'substr_compare', 'strrev', 'parse_url', 'http_build_query', 'header', 'headers_sent', 'setcookie', 'session_start',
    'session_id', 'session_destroy', 'session_unset', 'session_regenerate_id', 'session_cache_expire', 'session_cache_limiter',
    'session_set_cookie_params', 'session_get_cookie_params', 'session_name', 'session_module_name', 'session_save_path',
    'session_encode', 'session_decode', 'session_gc', 'session_commit', 'session_status', 'session_register_shutdown',
    'session_create_id', 'session_reset', 'session_abort', 'get_headers', 'get_meta_tags', 'http_response_code',
    'setrawcookie', 'header_register_callback', 'header_remove', 'headers_list', 'getallheaders', 'apache_request_headers',
    'apache_response_headers', 'get_browser', 'php_sapi_name', 'php_uname', 'phpcredits', 'phpinfo', 'version_compare',
    'zend_version', 'phpversion', 'gc_collect_cycles', 'gc_enable', 'gc_disable', 'gc_enabled', 'gc_status', 'gc_mem_caches',
    'gc_collect_cycles', 'gc_cache_create', 'gc_cache_destroy', 'gc_cache_clear', 'gc_cache_info', 'dl', 'extension_loaded',
    'get_loaded_extensions', 'get_extension_funcs', 'get_defined_functions', 'get_defined_vars', 'get_defined_constants',
    'get_included_files', 'get_required_files', 'sys_get_temp_dir', 'get_cfg_var', 'get_magic_quotes_gpc', 'get_magic_quotes_runtime',
    'error_reporting', 'restore_error_handler', 'restore_exception_handler', 'set_error_handler', 'set_exception_handler',
    'trigger_error', 'user_error', 'error_get_last', 'error_clear_last', 'debug_backtrace', 'debug_print_backtrace',
    'gc_collect_cycles', 'memory_get_usage', 'memory_get_peak_usage', 'highlight_file', 'highlight_string', 'php_strip_whitespace',
    'ini_get', 'ini_get_all', 'ini_set', 'ini_restore', 'get_include_path', 'set_include_path', 'restore_include_path',
    'parse_ini_file', 'parse_ini_string', 'assert', 'assert_options', 'class_exists', 'interface_exists', 'trait_exists', 'function_exists',
    'method_exists', 'property_exists', 'constant', 'define', 'defined', 'get_class', 'get_called_class', 'get_parent_class',
    'is_subclass_of', 'get_class_methods', 'get_class_vars', 'get_object_vars', 'get_declared_classes', 'get_declared_interfaces',
    'get_declared_traits', 'spl_classes', 'spl_autoload_functions', 'spl_autoload_register', 'spl_autoload_unregister',
    'spl_autoload_call', 'spl_autoload_extensions', 'iterator_to_array', 'iterator_count', 'iterator_apply', 'array_walk',
    'array_walk_recursive', 'array_filter', 'array_map', 'array_reduce', 'array_merge', 'array_merge_recursive', 'array_keys',
    'array_values', 'array_key_exists', 'array_search', 'array_flip', 'array_reverse', 'array_diff', 'array_diff_assoc',
    'array_diff_key', 'array_diff_uassoc', 'array_diff_ukey', 'array_udiff', 'array_udiff_assoc', 'array_udiff_uassoc', 'array_intersect',
    'array_intersect_assoc', 'array_intersect_key', 'array_intersect_uassoc', 'array_intersect_ukey', 'array_uintersect',
    'array_uintersect_assoc', 'array_uintersect_uassoc', 'array_multisort', 'array_push', 'array_pop', 'array_shift', 'array_unshift',
    'array_slice', 'array_splice', 'array_pad', 'array_chunk', 'array_fill', 'array_fill_keys', 'array_sum', 'array_product', 'array_rand',
    'array_unique', 'array_column', 'array_combine', 'array_change_key_case', 'array_replace', 'array_replace_recursive', 'sort',
    'rsort', 'asort', 'arsort', 'ksort', 'krsort', 'usort', 'uasort', 'uksort', 'natsort', 'natcasesort', 'array_unique',
    'array_multisort', 'max', 'min', 'range', 'array_column', 'array_map', 'array_walk', 'array_walk_recursive', 'extract',
    'compact', 'func_get_arg', 'func_get_args', 'func_num_args', 'register_tick_function', 'unregister_tick_function', 'call_user_func',
    'call_user_func_array', 'call_user_method', 'call_user_method_array', 'forward_static_call', 'forward_static_call_array',
    'create_function', 'eval', 'is_callable', 'on', 'rtrim', 'ltrim', 'in_array', 'return', 'in', 'select', 'from', 'where', 
    'group by', 'order by', 'join', 'on', 'insert', 'into', 'values', 'update', 'set', 'delete', 'distinct', 'count', 'avg',
    'sum', 'max', 'min', 'as', 'is null', 'not null', 'like', 'between', 'in', 'or', 'and', 'exists', 'having', 'union', '__construct'
]

def parse_arguments():
    parser = argparse.ArgumentParser(
        description='Traverse a PHP Symfony project.')
    parser.add_argument('-i', '--input_directory', required=True,
                        help='Input directory of the PHP Symfony project.')
    parser.add_argument('-x', '--exclude_directories', nargs='+',
                        default=[], help='List of directories to exclude')
    parser.add_argument('-n', '--depth', type=int, default=1,
                        help='Depth of the call tree (default: 1).')
    parser.add_argument('-o', '--output_file', default='output.md',
                        help='Output Markdown file (default: output.md).')
    return parser.parse_args()

def convert_to_file(file):
    path_parts = file.split("\\")
    return f'{"/".join(path_parts)}'

def convert_to_path_file(include, input_directory):
    path_parts = include.split("\\")
    return f'{input_directory}{"/".join(path_parts[:-1])}/{path_parts[-1]}.php'

def convert_to_path(namespace, input_directory):
    path_parts = namespace.split("\\")
    return f'{input_directory}{"/".join(path_parts)}'

def extract_includes(file_content, input_directory):
    includes = re.findall(r'use\s+([\w\\]+);', file_content)
    includes_paths = [convert_to_path_file(
        include, input_directory) for include in includes]
    return includes_paths

def extract_namespaces(file_content, input_directory):
    namespaces = re.findall(r'namespace\s+([\w\\]+)', file_content)
    namespace_paths = [convert_to_path(
        namespace, input_directory) for namespace in namespaces]
    return namespace_paths

def get_namespace_files(namespace, input_directory):
    namespace_path = convert_to_path(namespace, input_directory)
    namespace_files = []

    for root, _, files in os.walk(namespace_path):
        for file in files:
            if file.endswith('.php'):
                filepath = convert_to_file(f'{root}/{file}')
                namespace_files.append(filepath)

    return namespace_files


def get_all_namespace_files(namespaces, input_directory):
    all_namespace_files = []

    for namespace in namespaces:
        namespace_files = get_namespace_files(namespace, input_directory)
        all_namespace_files.extend(namespace_files)

    return all_namespace_files

# TODO: fix below, currently only looks at current file
def find_function_definitions(file_content):
    return re.findall(r'(public|private|protected)?\s*function\s+(\w+)\s*\(', file_content)

def find_function_calls(function_body):
    return re.findall(r'\b(\w+)\s*\(', function_body)


def find_nested_calls(function_name, file_content, depth, input_directory):
    if depth == 0:
        return []

    function_definition_regex = fr'(public|private|protected)?\s*function\s+{function_name}\s*\(([^\)]*)\)\s*\{{([^{{]+)\}}'
    matches = re.search(function_definition_regex, file_content)
    if not matches:
        return []

    function_body = matches.group(3)
    function_calls = find_function_calls(function_body)

    user_defined_calls = [
        call for call in function_calls if call not in COMMON_PHP_BUILT_INS]

    nested_calls = []
    for call in user_defined_calls:
        nested_calls.extend(find_nested_calls(
            call, file_content, depth - 1, input_directory))

    return user_defined_calls + nested_calls

def load_file_content(filepath):
    with open(filepath, encoding='utf-8') as file:
        return file.read()

def find_user_defined_function_definition(function_call, filepaths):
    for filepath in filepaths:
        try:
            file_content = load_file_content(filepath)
            function_definitions = find_function_definitions(file_content)
            for _, function_name in function_definitions:
                if function_name == function_call:
                    return filepath, file_content
        except:
            logging.info(
                f'Failed to load {filepath}, it\'s probably not in the input path')
            return None, None

    return None, None

def extract_call_tree(file_content, depth, input_directory):
    call_tree = []

    function_calls = find_function_calls(file_content)
    user_defined_calls = [
        call for call in function_calls if call.lower() not in COMMON_PHP_BUILT_INS]

    for call in user_defined_calls:
        includes = extract_includes(file_content, input_directory)
        namespaces = extract_namespaces(file_content, input_directory)
        namespaces_files = get_all_namespace_files(namespaces, input_directory)
        filepaths_to_search = includes + namespaces_files

        definition_filepath, definition_file_content = find_user_defined_function_definition(
            call, filepaths_to_search)

        if definition_file_content:
            nested_calls = find_nested_calls(
                call, definition_file_content, depth - 1, input_directory)
            call_tree.append((call, nested_calls, definition_filepath))

    return call_tree

def process_file(filepath, depth, input_directory):
    logging.warning(f'Parsing/traversing {filepath}')
    with open(filepath, encoding='utf-8') as file:
        content = file.read()

    includes = extract_includes(content, input_directory)
    namespaces = extract_namespaces(content, input_directory)
    call_tree = extract_call_tree(content, depth, input_directory)

    return includes, namespaces, call_tree

def generate_output(output_file, input_directory, depth, exclude):
    with open(output_file, 'w', encoding='utf-8') as outfile:
        outfile.write('<?xml version="1.0" encoding="UTF-8"?>\n')
        outfile.write('<opml version="2.0">\n')
        outfile.write('\t<head>\n')
        outfile.write('\t\t<title>Research</title>\n')
        outfile.write('\t</head>\n')
        outfile.write('\t<body>\n')
        outfile.write(f'\t\t<outline text="{input_directory.split("/")[-1]}" url="{input_directory}">\n')
        for root, dirs, files in os.walk(input_directory):
            dirs[:] = [d for d in dirs if d not in exclude]
            for file in files:
                if file.endswith('.php'):
                    filepath = convert_to_file(f'{root}/{file}')
                    includes, namespaces, call_tree = process_file(
                        filepath, depth, input_directory)

                    # outfile.write(f'* {filepath}:\n')
                    outfile.write(f'\t\t\t<outline text="{filepath.split("/")[-1]}" url="{filepath}">\n')
                    # outfile.write('    * Includes:\n')
                    outfile.write(f'\t\t\t\t<outline text="Includes">\n')
                    for include in includes:
                        # outfile.write(f'        * {include}\n')
                        outfile.write(f'\t\t\t\t\t<outline text="{include.split("/")[-1]}" url="{include}"/>\n')
                    outfile.write(f'\t\t\t\t</outline>\n')

                    # outfile.write('    * Namespaces:\n')
                    outfile.write(f'\t\t\t\t<outline text="Namespaces">\n')
                    for namespace in namespaces:
                        if namespace:
                            # outfile.write(f'        * {namespace}\n')
                            outfile.write(f'\t\t\t\t\t<outline text="{namespace.split("/")[-1]}" url="{namespace}"/>\n')
                        else:
                            # outfile.write(f'        * Not found\n')
                            pass
                    outfile.write(f'\t\t\t\t</outline>\n')

                    # outfile.write('    * Call Tree:\n')
                    outfile.write(f'\t\t\t\t<outline text="Call Tree">\n')
                    for index, (function, called_functions, definition_file_path) in enumerate(call_tree, start=1):
                        # outfile.write(f'\t\t{index}. {function} - {definition_file_path}\n')
                        outfile.write(f'\t\t\t\t\t<outline text="{index}. {function} - {definition_file_path.split("/")[-1]}" url="{definition_file_path}/">\n')
                        for idx, called_function in enumerate(called_functions, start=1):
                            # outfile.write(f'            {idx}. {called_function} - {definition_file_path}\n')
                            outfile.write(f'\t\t\t\t\t\t<outline text="{idx}. {called_function} - {definition_file_path.split("/")[-1]}" url="{definition_file_path}/">\n')
                    outfile.write(f'\t\t\t\t</outline>\n')

                    # outfile.write('\n')
                    outfile.write(f'\t\t\t</outline>\n')
        outfile.write(f'\t\t</outline>\n')
        outfile.write('\t</body>\n')
        outfile.write('\t</opml>')

    logging.info(f'Output written to: {output_file}')

def main():
    args = parse_arguments()
    generate_output(args.output_file, args.input_directory,
                    args.depth, args.exclude_directories)

if __name__ == '__main__':
    main()
