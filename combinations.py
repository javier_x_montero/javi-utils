#!/usr/bin/env python3

import argparse
from itertools import combinations

def main():
    parser = argparse.ArgumentParser(description="Generate combinations of a given list of strings.")
    parser.add_argument('strings', metavar='S', type=str, nargs='+', help='list of input strings')
    parser.add_argument('-c', '--comb_length', type=int, default=2, help='combination length (default: 2)')
    args = parser.parse_args()

    input_strings = args.strings
    comb_length = args.comb_length

    if comb_length > len(input_strings):
        print("Error: Combination length must be less than or equal to the number of input strings.")
        return

    result = list(combinations(input_strings, comb_length))
    
    for item in result:
        print(' '.join(item))

if __name__ == "__main__":
    main()
