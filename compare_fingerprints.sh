#!/usr/bin/env bash

usage() {
  echo "Usage: $0 -a host1 -b host2"
  echo "  -a  First host to compare."
  echo "  -b  Second host to compare."
  echo "  -h  Display help."
  exit 1
}

while getopts "a:b:h" opt; do
  case $opt in
    a) host1="$OPTARG" ;;
    b) host2="$OPTARG" ;;
    h) usage ;;
    *) usage ;;
  esac
done

if [ -z "$host1" ] || [ -z "$host2" ]; then
  usage
fi

ssh-keyscan "${host1}" > "${host1}_key.pub" 2>/dev/null
ssh-keyscan "${host2}" > "${host2}_key.pub" 2>/dev/null

fingerprint1=$(ssh-keygen -lf "${host1}_key.pub" | awk '{print $2}')
fingerprint2=$(ssh-keygen -lf "${host2}_key.pub" | awk '{print $2}')

if [ "${fingerprint1}" == "${fingerprint2}" ]; then
  echo "${host1} and ${host2} match."
fi

rm "${host1}_key.pub" "${host2}_key.pub"
