#!/usr/bin/env python3

import argparse
import socket

def lookup_ips_and_hostnames(target):
    # Determine if the target is an IP or a hostname
    is_ip = True
    try:
        socket.inet_aton(target)
    except socket.error:
        is_ip = False

    if is_ip:
        try:
            hostname, aliases, ip_addresses = socket.gethostbyaddr(target)
            all_hostnames = [hostname] + aliases
        except socket.error as err:
            print(f"Error: {err}")
            return
    else:
        hostname = target
        try:
            ip_addresses = [addr[-1][0] for addr in socket.getaddrinfo(hostname, None)]
        except socket.error as err:
            print(f"Error: {err}")
            return

        all_hostnames = [hostname]
        for ip in ip_addresses:
            try:
                host, aliases, _ = socket.gethostbyaddr(ip)
                all_hostnames += [host] + aliases
            except socket.error:
                pass

    all_hostnames = list(set(all_hostnames))
    ip_addresses = list(set(ip_addresses))

    print(f"Hostnames: {', '.join(all_hostnames)}")
    print(f"IP Addresses: {', '.join(ip_addresses)}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Lookup IPs and hostnames for a given target")
    parser.add_argument("target", help="IP address or hostname to lookup")
    args = parser.parse_args()

    lookup_ips_and_hostnames(args.target)
